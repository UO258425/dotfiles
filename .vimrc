set nocompatible

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

call vundle#end()
filetype plugin indent on

let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1
nmap <F2> :NERDTreeToggle<CR>
set number
syntax on
set tabstop=4
set autoindent
set shiftwidth=4
set expandtab
set smarttab
set textwidth=80
set ruler
set wildmenu
set scrolloff=5
set hlsearch
set incsearch
set showmatch
set autoread
set laststatus=2
set noerrorbells
syntax enable
colorscheme molokai
set cursorline
inoremap kj <C-C>
inoremap jk <C-C>

let g:airline_theme='behelit'
